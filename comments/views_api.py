from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest, HttpResponseForbidden, Http404
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.utils.crypto import get_random_string
from django.contrib.auth.decorators import login_required
from django.dispatch import Signal
from django.dispatch import receiver
from comments.signals import signal_new_comment, signal_update_comment, signal_delete_comment, signal_vote, signal_flag
from comments.constants import Constants
from comments.models import Thread, Comment, Vote, Profile, Flag
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from django.conf import settings
from urllib.parse import urlparse
import uuid
import re
import base64, datetime, re, logging, mimetypes, hmac, hashlib, json
from django.views.decorators.cache import cache_page
from django.core.cache import cache

logger = logging.getLogger(__name__)

"""
================================================================================================================================================================
COMMENT API ENDPOINTS
================================================================================================================================================================
"""

def get_cf_json(cf_auth):
	auth_items = cf_auth.split()
	cf_json = json.loads(base64.b64decode(auth_items[0]).decode('utf-8'))
	return cf_json

def is_authorized(cf_auth):
	auth_items = cf_auth.split()
	cf_data = auth_items[0]
	cf_signature = auth_items[1]
	cf_timestamp = auth_items[2]
	cf_json = json.loads(base64.b64decode(cf_data).decode('utf-8'))
	cf_message = cf_data + ' ' + str(cf_timestamp)
	cf_verify_signature = hmac.HMAC(settings.API_SECRET.encode('utf-8'), cf_message.encode('utf-8'), hashlib.sha256).hexdigest()

	if cf_signature != cf_verify_signature:
		# not a valid request
		logger.warn("cf_auth_failure: " + cf_json)
		return False
	return True

def init(cf_json):
	profile = None
	if cf_json['profile_id'] != "anonymous" and cf_json['profile_id'] is not None:
		try:
			profile = Profile.objects.get(profile_id=cf_json['profile_id'])
			change=False
			if profile.display_name != cf_json['display_name']:
				profile.display_name = cf_json['display_name']
				change=True
			if profile.icon_url != cf_json['icon_url']:
				profile.icon_url = cf_json['icon_url']
				change=True
			if change:
				profile.save()
		except ObjectDoesNotExist:
			logger.warn('profile does not exist: ' + cf_json['profile_id'])
			profile = Profile(profile_id=cf_json['profile_id'], display_name=cf_json['display_name'], icon_url=cf_json['icon_url'])
			profile.save()
		except Exception as e:
			logger.critical(e)

	owner = None
	try:
		owner = Profile.objects.get(profile_id=cf_json['owner_id'])
	except ObjectDoesNotExist:
		owner = Profile(profile_id=cf_json['owner_id'])
		owner.save()

	thread = None 
	try:
		thread = Thread.objects.get(thread_id=cf_json['thread_id'])
	except ObjectDoesNotExist:
		thread = Thread(owner=owner, thread_id=cf_json['thread_id'])
		thread.save()
	except Exception as e:
		logger.critical(e)

	return True
		
@csrf_exempt
@require_http_methods(["POST"])
def get_comments(request):
	cf_json = get_cf_json(request.POST.get('cf_auth'))
	init(cf_json)

	commentCount = int(request.POST.get('commentCount'))
	
	thread_id = cf_json['thread_id']
		
	comments = cache.get(thread_id)
	if not comments:
		try:
			comments = Comment.objects.filter(thread=thread_id, state__gte=Constants.STATE_VISIBLE).order_by('date_created')
			cache.set(thread_id, comments, 600)
			cache.set('count_'+thread_id, comments.count(), 600)
			logger.warn("db query " + str(len(comments)))
		except Exception as e:
			logger.warn(e)

	commentsArray=[]	
	for i, c in enumerate(comments):
		parent = c.parent if c.parent is not None else None
		
		profile = None
		if cf_json['profile_id'] != 'None' and cf_json['profile_id'] != 'anonymous':
			try:
				profile = Profile.objects.get(profile_id=cf_json['profile_id'])
			except Exception as e:
				logger.critical(e)

		created_by_current_user = False
		if c.creator.profile_id == cf_json['profile_id']:
			created_by_current_user = True

		user_has_upvoted = False
		if c.up_vote_count > 0 and profile:
			try:
				Vote.objects.get(comment=c, voter=profile)
				user_has_upvoted = True
			except ObjectDoesNotExist:
				user_has_upvoted = False
			except Exception as e:
				logger.warn(e)
				
		modified = None
		if c.modified:
			modified = str(c.modified)
		
		content = c.content
		if c.state == Constants.STATE_PLACEHOLDER:
			created_by_current_user = False
			content = "[comment removed]"

		attachments = []
		is_new = False
		if commentCount != 0 and i >= commentCount:
			is_new = True
		comment = { "id": c.comment_id, "parent": parent, "created": str(c.date_created), "modified": modified, "content": content, "pings": [], "creator": c.creator.profile_id, "fullname": c.creator.display_name, "created_by_admin": False, "created_by_current_user": created_by_current_user, "upvote_count": c.up_vote_count, "user_has_upvoted": user_has_upvoted, "is_new": is_new, "profile_picture_url": c.creator.icon_url }
		if c.file_url:
			attachment = {}
			attachment['id'] = 1
			attachment['file'] = c.file_url
			attachment['mime_type'] = c.file_mime_type
			attachments.append(attachment)
			
		comment['attachments'] = attachments
		commentsArray.append(comment)
	
	return JsonResponse(commentsArray, safe=False)
	
@csrf_exempt
@require_http_methods(["POST"])
def create_comment(request):

	cf_json = get_cf_json(request.POST.get('cf_auth'))

	data = { 'thread_id': cf_json['thread_id'], 'id': get_random_string(length=36), 'parent': request.POST.get('commentData[parent]'), 'content': request.POST.get('commentData[content]'), 'author': cf_json['profile_id'], 'created': request.POST.get('commentData[created]'), 'modified': request.POST.get('commentData[modified]') }
	
	file_url = None
	file_mime_type = None
	attachments = []
	try:
		file_url = re.search("(?P<url>https?://[^\s]+)", data['content']).group("url")
		if file_url:
			file_mime_type = mimetypes.guess_type(file_url)
			logger.warn(file_mime_type)
			domain = urlparse(file_url).netloc
			print(domain) 
			tld = re.sub(r'^\w+\.', '', domain)
			if tld in settings.MEME_DOMAINS:
				data['file_url'] = file_url
				data['file_mime_type'] = file_mime_type[0]
			else:
				file_mime_type = None
				file_url = None
	except Exception as e:
		logger.warn(e)
	
	if is_authorized(request.POST.get('cf_auth')):
		signal_new_comment.send_robust(sender=1, data=data)

	parent = None
	if data["parent"]:
		parent = data["parent"]
		
	comment = { "id": data['id'], "parent": parent, "created": data["created"], "modified": data["modified"], "content": data["content"], "pings": [], "creator": cf_json['profile_id'], "fullname": cf_json['display_name'], "profile_picture_url": cf_json['icon_url'], "created_by_admin": False, "created_by_current_user": True, "upvote_count": 0, "user_has_upvoted": False, "is_new": False }
	if file_mime_type:
		attachment = {}
		attachment['mime_type'] = file_mime_type[0]
		attachment['file'] = file_url
		attachments.append(attachment)
	comment['attachments'] = attachments

	return JsonResponse(comment)
	

@csrf_exempt
@require_http_methods(["POST"])
def update_comment(request):
	cf_json = get_cf_json(request.POST.get('cf_auth'))

	author = None
	try:
		author = Profile.objects.get(profile_id=cf_json['profile_id'])
	except Exception as e:
		logger.warn(e)
	data = { 'thread_id': cf_json['thread_id'], 'id': request.POST.get('commentData[id]'), 'parent': request.POST.get('commentData[parent]'), 'content': request.POST.get('commentData[content]'), 'author': author }
	if is_authorized(request.POST.get('cf_auth')):
		signal_update_comment.send_robust(sender=1, data=data)
	return JsonResponse({'success': True})
	
@csrf_exempt
@require_http_methods(["POST"])
def delete_comment(request):
	cf_json = get_cf_json(request.POST.get('cf_auth'))

	author = None
	try:
		author = Profile.objects.get(profile_id=cf_json['profile_id'])
	except Exception as e:
		logger.warn(e)
	data = { 'thread_id': cf_json['thread_id'], 'id': request.POST.get('commentData[id]'), 'author': author }
	if is_authorized(request.POST.get('cf_auth')):
		signal_delete_comment.send_robust(sender=1, data=data)
	return JsonResponse({'success': True})
	
@csrf_exempt
@require_http_methods(["POST"])	
def upvote_comment(request):
	cf_json = get_cf_json(request.POST.get('cf_auth'))

	author = None
	try:
		author = Profile.objects.get(profile_id=cf_json['profile_id'])
	except Exception as e:
		logger.warn(e)
	data = { 'thread_id': cf_json['thread_id'], 'id': request.POST.get('commentData[id]'), 'author': author }
	if is_authorized(request.POST.get('cf_auth')):
		signal_vote.send_robust(sender=1, data=data)
	return JsonResponse({'success': True})

@csrf_exempt
@require_http_methods(["POST"])
def flag_comment(request):
	cf_json = get_cf_json(request.POST.get('cf_auth'))

	flagger = None
	try:
		flagger = Profile.objects.get(profile_id=cf_json['profile_id'])
	except Exception as e:
		logger.warn(e)
		return JsonResponse({'success': False})

	data = { 'id': request.POST.get('commentData[id]'), 'flagger': flagger, 'flag_reason': request.POST.get('commentData[flag_reason]') }
	if is_authorized(request.POST.get('cf_auth')):
		signal_flag.send_robust(sender=1, data=data)

	return JsonResponse({'success': True})

@csrf_exempt
@require_http_methods(["POST"])
def get_comment_count(request):
	cf_thread = request.POST.get('cf_thread', None)

	if cf_thread is None: 
		return JsonResponse({'commentCount': 0})

	count = cache.get('count_'+cf_thread)
	if count:
		return JsonResponse({'commentCount': count})

	return JsonResponse({'commentCount': 0})


"""
================================================================================================================================================================
COMMENT API RECIEVERS
================================================================================================================================================================
"""

@receiver(signal_new_comment)
def on_new_comment(sender, data, **kwargs):

	thread = None
	try:
		thread = Thread.objects.get(thread_id=data['thread_id'])
	except Exception as e:
		logger.warn(e)

	thread_id = thread_id=data['thread_id']
		
	# If this comment has a parent make sure it is a valid one	
	parent_id = None
	if data['parent']:
		try:
			parent_id=data['parent']
			Comment.objects.get(comment_id=parent_id, state=Constants.STATE_VISIBLE)
		except ObjectDoesNotExist:
			parent_id = None
		except Exception as e:
			parent_id = None
			logger.warn(e)
	else:
		parent_id = None

	profile = None
	try:
		profile = Profile.objects.get(profile_id=data['author'])
	except Exception as e:
		logger.warn(e)
		return

	if not profile.is_active:
		return
		
	comment = Comment(comment_id=data["id"], thread=thread, parent=parent_id, content=data['content'], creator=profile)
	
	try:
		comment.file_mime_type = data['file_mime_type']
		comment.file_url = data['file_url']
	except:
		logger.info('saving comment without media')
			
	try:
		comment.save()
	except Exception as e:
		logger.warn(e)

	try:
		cache.delete(thread.thread_id)
	except Exception as e:
		logger.warn(e)

	try:
		count = cache.get('count_'+thread_id)
		if count:
			count = count+1
			cache.set('count_'+thread.thread_id, count, 600)
	except Exception as e:
		logger.warn(e)

		
    
@receiver(signal_update_comment)
def on_update_comment(sender, data, **kwargs):
    #logger.info("signal_update_comment")
    
    comment = None
    try:
    	comment = Comment.objects.get( comment_id=data['id'], creator=data['author'] )
    except Exception as e:
    	logger.warn(e)
    
    if comment:
    	try:
    		comment.content = data['content']
    		comment.modified = datetime.datetime.now(tz=timezone.utc)
    		comment.save()
    	except Exception as e:
    		logger.warn(e)

    	try:
    		cache.delete(comment.thread.thread_id)
    	except Exception as e:
    		logger.warn(e)
    
    	
@receiver(signal_delete_comment)
def on_delete_comment(sender, data, **kwargs):
    logger.info("signal_delete_comment")
    
    comment = None
    try:
    	comment = Comment.objects.get( comment_id=data['id'], creator=data['author'] )
    except Exception as e:
    	logger.warn(e)
    
    if comment:
    	try:
    		if Comment.objects.filter(parent=comment.comment_id).count() > 0:
    			comment.state = Constants.STATE_PLACEHOLDER
    		else:
    			comment.state = Constants.STATE_DELETED # TODO: Split out for different deletion reasons
    			count = cache.get('count_'+comment.thread.thread_id)
    			count = count-1
    			cache.set('count_'+comment.thread.thread_id, count, 600)
    		comment.save()
    	except Exception as e:
    		logger.warn(e)

    	try:
    		cache.delete(comment.thread.thread_id)
    	except Exception as e:
    		logger.warn(e)
    
    
@receiver(signal_vote)
def on_vote_comment(sender, data, **kwargs):
    comment_id = data['id']
    vote_down = False
    comment = None
    try:
    	comment = Comment.objects.get(comment_id=comment_id)
    except Exception as e:
    	logger.warn(e)
    	
    try:
    	vote = Vote.objects.get(comment=comment, voter=data['author'])
    	vote.delete()
    	vote_down = True
    except ObjectDoesNotExist:
    	vote_down = False
    	try:
    		vote = Vote(comment=comment, voter=data['author'])
    		vote.save()
    	except:
    		logger.warn(e)
    except Exception as e:
    	logger.warn(e)
    	
    try:
    	if vote_down:
    		comment.up_vote_count = comment.up_vote_count - 1
    	else:
    		comment.up_vote_count = comment.up_vote_count + 1
    	comment.save()
    except:
    	logger.warn(e)

    try:
    	cache.delete(comment.thread.thread_id)
    except Exception as e:
    	logger.warn(e)


@receiver(signal_flag)
def on_flag_comment(sender, data, **kwargs):
	logger.info('signal_flag')
	if data['flag_reason'] == "":
		return

	comment = Comment.objects.get(comment_id=data['id'])
	flag = Flag(reason=data['flag_reason'], flagger=data['flagger'], comment=comment)
	flag.save()