from django.urls import path
from django.contrib import admin

from . import views

urlpatterns = [
    #path('', views.index, name='index'),
]

admin.site.site_header = "CommentFreely Admin"
admin.site.site_title = "CommentFreely Admin Portal"
admin.site.index_title = "Welcome to CommentFreely Admin Portal"